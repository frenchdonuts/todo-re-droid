package io.onedonut.utils

import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmQuery
import io.realm.RealmResults
import org.funktionale.either.Either
import rx.Observable
import rx.Scheduler
import rx.schedulers.Schedulers

/**
 * Created by frenchdonuts on 5/11/2016.
 */

// Read
fun <E : RealmModel> RealmQuery<E>.toObservable(): Observable<RealmResults<E>> =
        this.findAllAsync()
                .asObservable()
                .filter { it.isLoaded }

fun <E : RealmModel> RealmQuery<E>.find(): Observable<Either<String, RealmResults<E>>> =
        this.findAllAsync()
                .asObservable()
                .filter { it.isLoaded }
                .map {
                    if (it.isEmpty())
                        Either.Left<String, RealmResults<E>>("Query result is empty.")
                    else
                        Either.Right<String, RealmResults<E>>(it)
                }

// Write
fun <R> ((Realm) -> R).execute(realm: Realm, scheduler: Scheduler = Schedulers.io()): Observable<Either<String, R>> =
        Observable.create<Either<String, R>> {
            subscriber ->
            realm.executeTransaction {
                try {
                    val transactionReturnValue = this.invoke(it)
                    subscriber.onNext(Either.Right(transactionReturnValue));
                    subscriber.onCompleted()
                } catch(err: Exception) {
                    //
                    subscriber.onNext(Either.Left<String, R>(err.toString()));
                    subscriber.onCompleted()
                }
            }
        }
