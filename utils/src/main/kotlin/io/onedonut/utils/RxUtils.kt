package io.onedonut.utils

import io.onedonut.utils.base.BaseActivity
import io.onedonut.utils.base.BaseFragment
import org.funktionale.either.Either
import org.funktionale.utils.identity
import rx.Observable
import rx.Subscription

/**
 * Created by frenchdonuts on 3/11/16.
 */

fun Subscription.managedBy(baseActivity: BaseActivity) = baseActivity.compositeSubscription.add(this)
fun Subscription.managedBy(baseFragment: BaseFragment) = baseFragment.compositeSubscription.add(this)

infix fun <T, R> Observable<T>.then(next: (T) -> Observable<R>) = this.flatMap { next(it) }

fun <TEL, REL, TER, RER> Observable<Either<TEL, TER>>.rxEitherflatMap(next: (TER) -> Observable<Either<REL, RER>>) =
        this.flatMap {
            when (it) {
                is Either.Left -> rx.Observable.just(it.l)
                is Either.Right -> next(it.r)
            }
        }

// :: Observable<Either<TEL, TER>> -> ... ->Observable<Either<REL, RER>>
fun <TEL, REL, TER, RER> Observable<Either<TEL, TER>>.biflatMapRx(mapLeft: (TEL) -> Observable<Either<REL, RER>>, mapRight: (TER) -> Observable<Either<REL, RER>>) =
        this.flatMap {
            when (it) {
                is Either.Left -> mapLeft(it.l)
                is Either.Right -> mapRight(it.r)
            }
        }

// join :: Monad m => m (m a) -> m a
// join = (=<<) id

// join m = m >>= id
// Observable.join = this.flatMap(identity())
//   id :: m a -> m a
// >>= :: Monad m => m a -> (a -> m b) -> m b
// Either.flatMap :: Either a b -> (a
// Flatten an Observable<Observable<R>>
fun <R, T : Observable<R>> Observable<T>.join() = this.flatMap(org.funktionale.utils.identity())

/**
 * Right m >>= id = id m
 * Left e  >>= _ = Left e
 */
// :: Observable<Either<TEL, TER>> -> ... ->Observable<Either<REL, RER>>
fun <TEL, REL, TER, RER> Observable<Either<TEL, TER>>.bimapRx(mapLeft: (TEL) -> REL, mapRight: (TER) -> RER) =
        this.map {
            when (it) {
                is Either.Left -> org.funktionale.either.Either.Left<REL, RER>(mapLeft(it.l))
                is Either.Right -> org.funktionale.either.Either.Right<REL, RER>(mapRight(it.r))
            }
        }

fun <L, R1, R2> Observable<Either<L, R1>>.eitherThen(next: (R1) -> Observable<Either<L, R2>>) =
    this.flatMap {
        when (it) {
            is Either.Right -> next(it.r)
            is Either.Left -> rx.Observable.just(org.funktionale.either.Either.Left<L, R2>(it.l))
        }
    }

fun <TEL, REL, RER> emitError(left: TEL) = Observable.just(left)

fun <L, R, C> Either<L, R>.either(f1: (L) -> C, f2: (R) -> C) =
        when (this) {
            is Either.Left -> f1(this.left().get())
            is Either.Right -> f2(this.right().get())
        }

