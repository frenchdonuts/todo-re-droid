package io.onedonut.utils

import io.realm.annotations.RealmModule

/**
 * Created by frenchdonuts on 5/13/2016.
 */
@RealmModule(library = true, allClasses = true)
open class RealmModule {
}
