package io.onedonut.todo.repositories

import com.github.andrewoma.dexx.kollection.immutableListOf
import com.github.andrewoma.dexx.kollection.toImmutableList
import io.onedonut.todo.core.models.Task
import io.onedonut.todo.services.todo_db_service.models.TaskEntity
import io.onedonut.utils.biflatMapRx
import io.onedonut.utils.bimapRx
import io.onedonut.utils.execute
import io.onedonut.utils.find
import io.realm.Realm
import org.funktionale.either.Either
import org.funktionale.utils.identity
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


/**
 * Created by frenchdonuts on 5/10/2016.
 */
class TodoAppDBRepoImpl(val realm: Realm) : TodoAppDBRepo {
    //
    val TAG = this.javaClass.simpleName

    fun taskEntityToTask(taskEntity: TaskEntity): Task = Task(taskEntity.id, taskEntity.title, taskEntity.description, taskEntity.isComplete)
    fun taskEntityToTask(): (TaskEntity) -> Task = { taskEntityToTask(it) }

    // TODO: Query remote source if local data source fails
    override fun getTasksFromDB(): Observable<Either<String, List<Task>>> =
            realm.where(TaskEntity::class.java)
                    .find()
                    .bimapRx({ "No tasks in DB" }, { it.toImmutableList().map(taskEntityToTask()) })

    // TODO: Query remote source if local data source fails
    override fun getTask(id: String): Observable<Either<String, Task>> =
            realm.where(TaskEntity::class.java)
                    .equalTo("id", id)
                    .find()
                    .bimapRx({ "Task not found!" }, { taskEntityToTask(it.first()) })

    // TODO: Persist remotely as well
    override fun saveTask(task: Task, timesToRetry: Int): Observable<Either<String, Task>> =
            { realm: Realm ->
                //
                val taskEntity = TaskEntity()
                taskEntity.title = task.title
                taskEntity.description = task.description
                taskEntity.isComplete = task.isComplete
                realm.copyToRealmOrUpdate(taskEntity)

                taskEntity
            }.execute(realm)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .biflatMapRx(
                            {
                                // We're done trying
                                if (timesToRetry <= 0)
                                    Observable.just(Either.Left<String, Task>(it) as Either<String, Task>)
                                // Try again
                                else
                                    saveTask(task, timesToRetry - 1)
                            },
                            {
                                // Success!
                                Observable.just(Either.Right(taskEntityToTask(it)))
                            }
                    )

    private fun TaskEntity.updateWith(task: Task) {
        this.title = task.title
        this.description = task.description
        this.isComplete = task.isComplete
    }

    // TODO: Reflect changes remotely as well
    override fun updateTask(id: String, task1: Task): Observable<Either<String, Task>> =
            { realm: Realm ->
                //
                val taskEntity = realm.where(TaskEntity::class.java)
                        .equalTo("id", id)
                        .findFirst()
                val task0 = taskEntityToTask(taskEntity)

                taskEntity?.updateWith(task1)

                task0
            }.execute(realm)
                    .bimapRx(identity(), { it })

    // TODO: Reflect changes remotely as well
    override fun completeTask(id: String): Observable<Either<String, Task>> =
            { realm: Realm ->
                //
                val task = realm.where(TaskEntity::class.java)
                        .equalTo("id", id)
                        .findFirst()

                task.isComplete = true

                task
            }.execute(realm)
                    .bimapRx(identity(), { Task(it.id, it.title, it.description, it.isComplete) })

    // TODO: Reflect changes remotely as well
    override fun activateTask(id: String): Observable<Either<String, Task>> =
            { realm: Realm ->
                //
                val task = realm.where(TaskEntity::class.java)
                        .equalTo("id", id)
                        .findFirst()

                task.isComplete = true

                task
            }.execute(realm)
                    .bimapRx(identity(), { Task(it.id, it.title, it.description, it.isComplete) })

    // TODO: Reflect changes remotely as well
    override fun clearCompletedTasks(): Observable<Either<String, List<Task>>> =
            { realm: Realm ->
                //
                val tasks = realm.where(TaskEntity::class.java)
                        .equalTo("isComplete", true)
                        .findAll()
                val tasksToReturn = tasks.toImmutableList().map(taskEntityToTask())

                tasks.map { it.deleteFromRealm() }

                tasksToReturn
            }.execute(realm)

    // TODO: Reflect changes remotely as well
    override fun deleteAllTasks(): Observable<Either<String, List<Task>>> =
            { realm: Realm ->
                //
                val tasks = realm.where(TaskEntity::class.java)
                        .findAll()
                val tasksToReturn = tasks.toImmutableList().map(taskEntityToTask())

                tasks.map { it.deleteFromRealm() }

                tasksToReturn
            }.execute(realm)

    // TODO: Reflect changes remotely as well
    override fun deleteTask(id: String): Observable<Either<String, Task>> =
            { realm: Realm ->
                //
                var taskToReturn: Task?
                val taskQueryResult = realm.where(TaskEntity::class.java)
                        .equalTo("id", id)
                        .findAll()

                // The Exception we throw will make sure we don't return a null value for taskToReturn
                //   (look in the defn. of execute)
                if (taskQueryResult.isEmpty())
                    throw Exception("No such task!")
                else {
                    val task = taskQueryResult[0]
                    taskToReturn = taskEntityToTask(task)
                    task.deleteFromRealm()
                }

                taskToReturn
            }.execute(realm) as Observable<Either<String, Task>>
}