package io.onedonut.todo.repositories

import io.onedonut.todo.core.models.Task
import org.funktionale.either.Either
import rx.Observable

/**
 * Created by frenchdonuts on 5/11/2016.
 *
 * Should these things return Task or TaskEntity?
 * Task. The Core shouldn't know what data models the DB is using since that can change w/ DB impl.
 * Another way to see this is to consider what happens if we add a remote data source. It's clear
 *  that at some point, the user of this repository needs to receive ONE data type. The only one
 *  that makes sense is the data type that the Core uses
 */
interface TodoAppDBRepo {
    //

    fun getTasksFromDB(): Observable<Either<String, List<Task>>>

    fun getTask(id: String): Observable<Either<String, Task>>

    fun saveTask(task: Task, timesToRetry: Int = 3): Observable<Either<String, Task>>

    fun updateTask(id: String, task1: Task): Observable<Either<String, Task>>

    fun completeTask(id: String): Observable<Either<String, Task>>

    fun activateTask(id: String): Observable<Either<String, Task>>

    fun clearCompletedTasks(): Observable<Either<String, List<Task>>>

    fun deleteAllTasks(): Observable<Either<String, List<Task>>>

    fun deleteTask(id: String): Observable<Either<String, Task>>
}