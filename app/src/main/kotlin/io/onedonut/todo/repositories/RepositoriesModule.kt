package io.onedonut.todo.repositories

import dagger.Module
import dagger.Provides
import io.realm.Realm
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 5/9/2016.
 */
@Module
class RepositoriesModule {

    @Provides
    @Singleton
    fun provideTodoAppDBRepo(realm: Realm): TodoAppDBRepo = TodoAppDBRepoImpl(realm)
}