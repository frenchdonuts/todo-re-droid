package io.onedonut.todo

import dagger.Component
import io.onedonut.todo.core.AppState
import io.onedonut.re_droid.RDB
import io.onedonut.todo.views.add_edit_task.AddOrEditTaskFragment
import io.onedonut.todo.core.CoreModule
import io.onedonut.todo.repositories.RepositoriesModule
import io.onedonut.todo.services.ServicesModule
import io.onedonut.todo.services.todo_db_service.DBConfigModule
import io.onedonut.todo.views.statistics.StatisticsFragment
import io.onedonut.todo.views.task_detail.TaskDetailFragment
import io.onedonut.todo.views.tasks.TasksFragment
import io.onedonut.todo.views.*
import io.onedonut.todo.views.base.BaseActivity
import io.onedonut.todo.views.base.BaseFragment
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 4/25/16.
 */
@Singleton
@Component(modules = arrayOf(
        App::class,
        RDBModule::class,
        RepositoriesModule::class,
        DBConfigModule::class,
        ServicesModule::class,
        CoreModule::class
))
interface ApplicationComponent {
    //
/*    fun inject(activity: BaseActivity<MainActivity.FTag>)
    fun inject(fragment: BaseFragment)*/
    fun inject(activity: MainActivity)

    fun inject(fragment: TasksFragment)

    fun inject(fragment: AddOrEditTaskFragment)

    fun inject(fragment: TaskDetailFragment)

    fun inject(fragment: StatisticsFragment)

    fun rdb(): RDB<AppState>
}