package io.onedonut.todo

import dagger.Module
import dagger.Provides
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.middleware.logActions
import io.onedonut.re_droid.middleware.rx
import io.onedonut.todo.core.AppState
import io.onedonut.todo.core.rootReducer
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 4/25/16.
 */
@Module
class RDBModule {

    @Provides
    @Singleton
    fun provideRDB(): RDB<AppState> =
            RDB(
                    AppState(),
                    rootReducer,
                    // Run our rootReducer on the main thread
                    AndroidSchedulers.mainThread(),
                    // Middleware - gets evaluated from right to left
                    { (rx<AppState>(mutableMapOf()))(it) }, { logActions(it) }
            )

}