package io.onedonut.todo.views.task_detail

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.jakewharton.rxbinding.view.clicks
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.selectors.select
import io.onedonut.todo.App
import io.onedonut.todo.R
import io.onedonut.todo.core.AppState
import io.onedonut.todo.core.RxActions
import io.onedonut.todo.util.bindView
import io.onedonut.utils.base.BaseFragment
import io.onedonut.utils.managedBy
import javax.inject.Inject

/**
 * Created by pamelactan on 5/13/16.
 */
class TaskDetailFragment : BaseFragment() {
    //
    @Inject
    lateinit var rdb: RDB<AppState>
    @Inject
    lateinit var rxActions: RxActions

    val cbComplete: CheckBox by bindView(R.id.cbComplete_detail)
    val tvTitle: TextView by bindView(R.id.tvTitle_detail)
    val tvDescription: TextView by bindView(R.id.tvDescription_detail)
    val fabEdit: FloatingActionButton by bindView(R.id.fabEdit_detail)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rdb.select(taskDetailQuery)
                .map { it._1 }
                .subscribe {
                    //
                    cbComplete.isChecked = it.isComplete
                    tvTitle.text = it.title
                    tvDescription.text = it.description
                }.managedBy(this)

        cbComplete.clicks()
                .map { rdb.curAppState.taskBeingDisplayedInDetail }
                .doOnNext {
                    if (it.isComplete)
                        rdb.dispatch(rxActions.MarkTaskActive(it, TAG))
                    else
                        rdb.dispatch(rxActions.MarkTaskComplete(it, TAG))
                }
                .subscribe()
                .managedBy(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        App.graph.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            LayoutInflater.from(container!!.context).inflate(R.layout.fragment_task_detail, container, false)

    companion object {
        //
        val TAG = this.javaClass.simpleName
    }
}