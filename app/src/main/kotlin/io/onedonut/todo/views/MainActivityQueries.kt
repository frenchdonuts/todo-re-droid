package io.onedonut.todo.views

import io.onedonut.re_droid.selectors.Two
import io.onedonut.todo.core.AppState

/**
 * Created by frenchdonuts on 5/22/2016.
 */
val shouldToastUserQuery =
        { s: AppState ->
            Two(
                    s.toastMsg,
                    s.shouldToastUser)
        }
