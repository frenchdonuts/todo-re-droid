package io.onedonut.todo.views

import android.app.ActionBar
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.widget.Toast
import com.jakewharton.rxbinding.support.design.widget.itemSelections
import io.onedonut.re_droid.Action
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.selectors.select
import io.onedonut.todo.App
import io.onedonut.todo.R
import io.onedonut.todo.core.*
import io.onedonut.todo.util.bindView
import io.onedonut.utils.managedBy
import io.onedonut.todo.views.add_edit_task.AddOrEditTaskFragment
import io.onedonut.todo.views.statistics.StatisticsFragment
import io.onedonut.todo.views.task_detail.TaskDetailFragment
import io.onedonut.todo.views.tasks.TasksFragment
import io.onedonut.utils.base.BaseActivity
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by frenchdonuts on 4/24/16.
 *
 */
class MainActivity : BaseActivity() {
    //
    val TAG = MainActivity::class.java.simpleName
    val APPSTATE_KEY = "app_state_parcelable"

    @Inject
    lateinit var rdb: RDB<AppState>
    @Inject
    lateinit var rxActions: RxActions

    val toolbar: Toolbar by bindView(R.id.toolbar)
    val drawerLayout: DrawerLayout by bindView(R.id.drawerLayout)
    val navView: NavigationView by bindView(R.id.navView)

    // These are the Fragments this Activity manages
    val TasksF = TasksFragment.TAG
    val AddOrEditTaskF = AddOrEditTaskFragment.TAG
    val TaskDetailF = TaskDetailFragment.TAG
    val StatisticsF = StatisticsFragment.TAG

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.graph.inject(this);

        // Setup the toolbar
        setupToolbar(toolbar, actionBar)
        // Setup the navigation drawer
        setupNavDrawer()
        // Setup toasting mechanism
        setupToasts()

        // Restore saved instance state
        if (savedInstanceState != null) {
            // Restore AppState
            rdb.dispatch(RestoreState_(savedInstanceState.getParcelable<AppState>("state"), TAG))
        } else {
            // Load tasks from DB
            rdb.dispatch(rxActions.GetTasks(TAG))

            // Fragment to show on launch
            transactToFragment(TasksF, R.id.mainContent)
        }
    }


    private fun setupToolbar(toolbar: Toolbar, actionBar: ActionBar?) {
        //
        setSupportActionBar(toolbar);
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar?.setDisplayHomeAsUpEnabled(true);
    }

    val mapNavigationItemToFragment = hashMapOf(
            Pair(R.id.list_navigation_menu_item, TasksF),
            Pair(R.id.statistics_navigation_menu_item, StatisticsF))
    private fun setupNavDrawer() {
        //
        drawerLayout.setStatusBarBackground(R.color.colorPrimaryDark)

        val itemSelectionS = navView.itemSelections().share()
        itemSelectionS
                .map { it.itemId }
                .filter { currentFragment != mapNavigationItemToFragment[it] }
                .subscribe { transactToFragment(mapNavigationItemToFragment[it]!!, R.id.mainContent) }
                .managedBy(this)
    }

    private fun setupToasts() {
        rdb.select(shouldToastUserQuery)
                .filter { it._1.isNotEmpty() && it._2 }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    //
                    Toast.makeText(this, it._1, Toast.LENGTH_SHORT).show()
                    rdb.dispatch(UserToasted_(TAG))
                }.managedBy(this)
    }

    override fun handleAction(action: Action) {
        // This is where we will route Actions to appropriate Fragment transactions
        when (action) {
            is NavigateToTaskDetailScreen_ ->
                transactToFragment(TaskDetailF, R.id.mainContent)
            is NavigateToAddTaskScreen_ -> {
                transactToFragment(AddOrEditTaskF, R.id.mainContent)
            }
            else -> {
            }
        }
    }

    override fun makeFragment(ftag: String): Fragment =
        when (ftag) {
            TasksF -> TasksFragment()
            AddOrEditTaskF -> AddOrEditTaskFragment()
            TaskDetailF -> TaskDetailFragment()
            StatisticsF -> StatisticsFragment()
            else -> TasksFragment()
        }

    override fun onStart() {
        super.onStart()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        // Save AppState
        outState?.putParcelable(APPSTATE_KEY, rdb.curAppState)

        // Call superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState)
    }
}