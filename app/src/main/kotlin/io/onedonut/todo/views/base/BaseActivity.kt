package io.onedonut.todo.views.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import io.onedonut.re_droid.Action
import rx.subscriptions.CompositeSubscription

/**
 * Created by frenchdonuts on 4/24/16.
 *
 * Activities will manage:
 *  Fragment navigation
 *  Toasts
 */
abstract open class BaseActivity : AppCompatActivity() {
    //
    val compositeSubscription: CompositeSubscription = CompositeSubscription()
    var currentFragment: String = ""

    // setContentView(R.layout.activity_layout, App.graph.inject(this), etc.
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(this.javaClass.simpleName, "onCreate")
        super.onCreate(savedInstanceState)
    }

    // How Fragments can communicate w/ the MainActivity
    abstract open fun handleAction(action: Action)

    // Route Actions to appropriate FragmentTransactions
    abstract open fun makeFragment(fTag: String): Fragment

    // TODO: Parameterize by transition animation?
    fun transactToFragment(ftag: String, layoutId: Int) {
        //
        val fm: FragmentManager = supportFragmentManager
        var f: Fragment?

        f = fm.findFragmentByTag(ftag);
        if (f == null)
            f = makeFragment(ftag);

        fm.beginTransaction()
                .replace(layoutId, f, ftag)
                .addToBackStack(ftag)
                .commit();

        // TODO: Fuck. How about when the User presses the Back button? How do we update currentFragment then?
        // TODO: Uggh...looks like I may just end up putting the Fragment Stack in AppState after all...
        currentFragment = ftag
    }

    override fun onBackPressed() {
        //
        super.onBackPressed()
        currentFragment = getCurrentFragmentTag()
    }

    fun getCurrentFragmentTag(): String {
        //
        val fm = supportFragmentManager

        if (fm.backStackEntryCount > 0)
            return fm.getBackStackEntryAt(fm.backStackEntryCount - 1).name
        else
            return ""
    }

    // ? Subscribe to AppState here. ?
    override fun onStart() {
        Log.d(this.javaClass.simpleName, "onStart")
        super.onStart()
    }

    override fun onResume() {
        Log.d(this.javaClass.simpleName, "onResume")
        super.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        Log.d(this.javaClass.simpleName, "onSaveInstanceState")
        super.onSaveInstanceState(outState)
    }

    // Persist anything you have left to persist here.
    override fun onPause() {
        Log.d(this.javaClass.simpleName, "onPause")
        super.onPause()
    }

    override fun onStop() {
        Log.d(this.javaClass.simpleName, "onStop")
        compositeSubscription.clear();
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(this.javaClass.simpleName, "onDestroy")
        super.onDestroy()
    }
}