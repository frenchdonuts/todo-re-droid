package io.onedonut.todo.views.task_detail

import io.onedonut.re_droid.selectors.One
import io.onedonut.todo.core.AppState

/**
 * Created by frenchdonuts on 5/22/2016.
 */

val taskDetailQuery = { appState: AppState -> One(appState.taskBeingDisplayedInDetail) }
