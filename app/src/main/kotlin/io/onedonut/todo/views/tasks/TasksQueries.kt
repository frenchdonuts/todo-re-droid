package io.onedonut.todo.views.tasks

import io.onedonut.re_droid.selectors.One
import io.onedonut.todo.core.AppState

/**
 * Created by frenchdonuts on 5/22/2016.
 */

val tasksQuery = { appState: AppState -> One(appState.tasks) }
