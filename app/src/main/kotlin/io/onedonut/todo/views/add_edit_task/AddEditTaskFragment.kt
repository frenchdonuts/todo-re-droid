package io.onedonut.todo.views.add_edit_task

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.jakewharton.rxbinding.view.clicks
import io.onedonut.re_droid.RDB
import io.onedonut.todo.App
import io.onedonut.todo.R
import io.onedonut.todo.core.AppState
import io.onedonut.todo.core.RxActions
import io.onedonut.todo.util.bindView
import io.onedonut.todo.views.base.BaseFragment
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by pamelactan on 5/13/16.
 */
class AddOrEditTaskFragment : BaseFragment() {
    //
    @Inject
    lateinit var rdb: RDB<AppState>
    @Inject
    lateinit var rxActions: RxActions

    val etTitle: EditText by bindView(R.id.etTitle)
    val etDescription: EditText by bindView(R.id.etDescription)
    val fabDone: FloatingActionButton by bindView(R.id.fabDone)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fabDone.clicks()
                .throttleLast(100, TimeUnit.MILLISECONDS)
                .debounce(200, TimeUnit.MILLISECONDS)
                .subscribe {
                    //
                    rdb.dispatch(rxActions.AddTask(etTitle.text.toString(), etDescription.text.toString(), TAG))
                }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        App.graph.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            LayoutInflater.from(container!!.context).inflate(R.layout.fragment_add_or_edit_task, container, false)

    companion object {
        //
        val TAG = AddOrEditTaskFragment::class.java.simpleName
    }
}