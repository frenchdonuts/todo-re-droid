package io.onedonut.todo.views.base

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import org.jetbrains.anko.layoutInflater

/**
 * Created by frenchdonuts on 5/17/2016.
 */
abstract class BaseViewHolder<Model>(layoutResId: Int, parent: ViewGroup) :
    RecyclerView.ViewHolder(parent.context.layoutInflater.inflate(layoutResId, parent, false)){

    open abstract fun bind(model: Model): Unit
}

