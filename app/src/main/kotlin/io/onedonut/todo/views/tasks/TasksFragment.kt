package io.onedonut.todo.views.tasks

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.jakewharton.rxbinding.view.clicks
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.selectors.select
import io.onedonut.todo.App
import io.onedonut.todo.R
import io.onedonut.todo.core.AppState
import io.onedonut.todo.core.NavigateToAddTaskScreen_
import io.onedonut.todo.core.NavigateToTaskDetailScreen_
import io.onedonut.todo.core.RxActions
import io.onedonut.todo.core.models.Task
import io.onedonut.todo.util.bindView
import io.onedonut.todo.views.base.BaseActivity
import io.onedonut.todo.views.base.BaseFragment
import io.onedonut.todo.views.base.BaseViewHolder
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by frenchdonuts on 5/10/2016.
 */
class TasksFragment : BaseFragment() {
    //
    @Inject
    lateinit var rdb: RDB<AppState>
    @Inject
    lateinit var rxActions: RxActions

    val rvTasks: RecyclerView by bindView(R.id.rvTasks)
    val fabAddTask: FloatingActionButton by bindView(R.id.fabAddTodo)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        //
        super.onViewCreated(view, savedInstanceState)

        // Attach data to rvTasks
        rvTasks.adapter = Adapter(rdb, (activity as BaseActivity), rxActions)

        // Configure presentation of rvTasks
        rvTasks.layoutManager = LinearLayoutManager(context)


        fabAddTask.clicks()
                .throttleLast(100, TimeUnit.MILLISECONDS)
                .debounce(200, TimeUnit.MILLISECONDS)
                .map { NavigateToAddTaskScreen_(TAG) }
                .subscribe {
                    //
                    rdb.dispatch(it)
                    mainActivity.handleAction(it)
                }
    }

    class Adapter(val rdb: RDB<AppState>,
                  val mainActivity: BaseActivity,
                  val rxActions: RxActions) : RecyclerView.Adapter<Adapter.TaskViewHolder>() {
        //
        var tasks: List<Task>

        init {
            //
            tasks = listOf()
            rdb.select(tasksQuery)
                    .subscribe {
                        //
                        Log.d(TAG, "${it._1}")
                        tasks = it._1
                        notifyDataSetChanged()
                    }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder =
                TaskViewHolder(rdb, mainActivity, rxActions, parent)

        override fun onBindViewHolder(holder: TaskViewHolder, position: Int): Unit =
                holder.bind(tasks[position])

        override fun getItemCount(): Int {
            //
            Log.d(TAG, "itemCount: ${tasks.size}")
            return tasks.size
        }

        class TaskViewHolder(val rdb: RDB<AppState>,
                             val mainActivity: BaseActivity,
                             val rxActions: RxActions,
                             parent: ViewGroup) : BaseViewHolder<Task>(R.layout.item_task, parent) {
            //
            val cbComplete: CheckBox by bindView(R.id.cbComplete)
            val tvTitle: TextView by bindView(R.id.tvTitle)

            override fun bind(task: Task): Unit {
                //
                itemView.setOnClickListener {
                    //
                    val Navigate = NavigateToTaskDetailScreen_(task, TAG)
                    rdb.dispatch(Navigate)
                    mainActivity.handleAction(Navigate)
                }

                cbComplete.isChecked = task.isComplete
                cbComplete.setOnClickListener {
                    //
                    if (task.isComplete)
                        rdb.dispatch(rxActions.MarkTaskActive(task, TAG))
                    else
                        rdb.dispatch(rxActions.MarkTaskComplete(task, TAG))
                }

                tvTitle.text = task.title
            }
        }
    }

    override fun onAttach(context: Context?) {
        //
        super.onAttach(context)
        App.graph.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            LayoutInflater.from(container!!.context).inflate(R.layout.fragment_tasks, container, false)

    companion object {
        //
        val TAG = TasksFragment::class.java.simpleName
    }


}