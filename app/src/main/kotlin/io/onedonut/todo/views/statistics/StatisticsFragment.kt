package io.onedonut.todo.views.statistics

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.selectors.select
import io.onedonut.todo.App
import io.onedonut.todo.R
import io.onedonut.todo.core.AppState
import io.onedonut.todo.util.bindView
import io.onedonut.utils.base.BaseFragment
import io.onedonut.utils.managedBy
import javax.inject.Inject

/**
 * Created by frenchdonuts on 5/10/2016.
 */
class StatisticsFragment : BaseFragment() {
    //
    @Inject
    lateinit var rdb: RDB<AppState>

    val tvActiveTasksCount: TextView by bindView(R.id.tvActiveTasksCount)
    val tvCompletedTasksCount: TextView by bindView(R.id.tvCompletedTasksCount)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rdb.select(tasksQuery)
        .map { it._1 }
        .subscribe {
            //
            val completedTasksCount = it.filter { it.isComplete }.size
            val activeTasksCount = it.size - completedTasksCount

            tvActiveTasksCount.text = "Active tasks: $activeTasksCount"
            tvCompletedTasksCount.text = "Completed tasks: $completedTasksCount"
        }.managedBy(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            LayoutInflater.from(container!!.context).inflate(R.layout.fragment_statistics, container, false)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        App.graph.inject(this)
    }

    companion object {
        val TAG = this.javaClass.simpleName
    }
}