package io.onedonut.todo

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.util.Log
import dagger.Module
import dagger.Provides
import io.onedonut.todo.core.AppState
import io.onedonut.todo.services.ServicesModule
import io.onedonut.todo.services.todo_db_service.DBConfigModule
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 4/24/16.
 */
@Module
class App : Application() {
    //
    @Provides
    @Singleton
    fun provideApplicationContext(): Context = this

    companion object {
        @JvmStatic lateinit var graph: ApplicationComponent
    }

    lateinit var appState: AppState
    override fun onCreate() {
        //
        super.onCreate()

        // Instantiate dependency injection graph
        graph = DaggerApplicationComponent.builder()
                .app(this)
                .rDBModule(RDBModule())
                .dBConfigModule(DBConfigModule())
                .servicesModule(ServicesModule())
                .build()

        // Setup AppState logging
        graph.rdb().appStates().subscribe { Log.i("AppState", "$it") }
    }

    override fun attachBaseContext(base: Context?) {
        //
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}