package io.onedonut.todo.services.todo_db_service.models

import io.realm.annotations.RealmModule

/**
 * Created by pamelactan on 5/13/16.
 */
@RealmModule(classes = arrayOf(
        TaskEntity::class
))
open class SchemaModule {
}