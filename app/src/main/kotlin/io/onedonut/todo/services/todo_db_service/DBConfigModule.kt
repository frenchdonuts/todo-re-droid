package io.onedonut.todo.services.todo_db_service

import android.content.Context
import dagger.Module
import dagger.Provides
import io.onedonut.todo.services.todo_db_service.models.SchemaModule
import io.realm.RealmConfiguration

/**
 * Created by frenchdonuts on 5/10/2016.
 */
@Module
class DBConfigModule {
    //
    @Provides
    fun provideRealmConfig(context: Context): RealmConfiguration {
        return RealmConfiguration.Builder(context)
                .modules(SchemaModule())
                .deleteRealmIfMigrationNeeded()
                .build()
    }
}
