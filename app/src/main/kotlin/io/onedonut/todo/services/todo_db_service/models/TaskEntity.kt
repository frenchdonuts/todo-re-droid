package io.onedonut.todo.services.todo_db_service.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*


/**
 * Created by frenchdonuts on 5/10/2016.
 */
@RealmClass
open class TaskEntity() : RealmObject() {
    // This id will be automatically generated
    @PrimaryKey
    lateinit var id: String

    var title: String = ""

    var description: String = ""

    var isComplete: Boolean = false

    init {
        id = UUID.randomUUID().toString()
    }
}
