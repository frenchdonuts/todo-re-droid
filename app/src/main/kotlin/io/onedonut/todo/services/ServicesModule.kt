package io.onedonut.todo.services

import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 4/25/16.
 */
@Module
class ServicesModule {

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        //
        val client = OkHttpClient().newBuilder()
            .build()

        return client
    }

    @Provides
    fun provideRealm(realmConfiguration: RealmConfiguration): Realm {
        return Realm.getInstance(realmConfiguration)
    }
}