package io.onedonut.todo.core

import dagger.Module
import dagger.Provides
import io.onedonut.re_droid.RDB
import io.onedonut.todo.repositories.TodoAppDBRepo
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 5/12/2016.
 */
@Module
class CoreModule {

    @Provides
    @Singleton
    fun provideRxActions(rdb: RDB<AppState>, todoAppDBRepo: TodoAppDBRepo) = RxActions(rdb, todoAppDBRepo)
}