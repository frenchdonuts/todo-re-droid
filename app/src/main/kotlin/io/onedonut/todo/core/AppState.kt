package io.onedonut.todo.core

import com.github.andrewoma.dexx.kollection.immutableListOf
import io.onedonut.todo.core.models.Task
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

/**
 * Created by frenchdonuts on 3/2/16.
 *
 */
@PaperParcel
data class AppState(val tasks: List<Task> = immutableListOf(), // TODO: Test if this is ignored by PaperParcel. Why doesn't @Transient work?
                    val taskBeingDisplayedInDetail: Task = Task(),
                    val fetchTaskError: String = "",
                    val toastMsg: String = "",
                    val shouldToastUser: Boolean = false) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelAppState.CREATOR
    }
}
