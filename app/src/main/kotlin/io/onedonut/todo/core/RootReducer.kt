package io.onedonut.todo.core

import com.github.andrewoma.dexx.kollection.immutableListOf
import io.onedonut.re_droid.Action

/**
 * Created by frenchdonuts on 4/25/16.
 */

val rootReducer: (Action, AppState) -> AppState = {
    action, appState ->
    when (action) {
    // Todo App
        is AddTasksFromRepo_ -> appState.copy(tasks = action.tasks)
        is AddTask_ -> appState.copy(
                tasks = appState.tasks + immutableListOf(action.task)
        )

        is UpdateTask_ -> appState.copy(
                tasks = appState.tasks.map { if (it.id == action.task0.id) action.task1 else it }
        )
        is RemoveTask_ -> appState.copy(
                tasks = appState.tasks.filter { it.id != action.task.id }
        )

        is NavigateToAddTaskScreen_ -> appState
        is NavigateToTaskDetailScreen_ ->
            appState.copy(taskBeingDisplayedInDetail = action.task)

        is UserToasted_ -> appState.copy(
                shouldToastUser = false
        )

        // Restore state from parcel
        is RestoreState_ -> action.state

        // Ignore
        else -> appState
    }
}

