package io.onedonut.todo.core

import android.util.Log
import com.github.andrewoma.dexx.kollection.immutableListOf
import com.github.andrewoma.dexx.kollection.toImmutableList
import io.onedonut.re_droid.Action
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.middleware.RxEffect
import io.onedonut.todo.core.models.Task
import io.onedonut.todo.repositories.TodoAppDBRepo
import io.onedonut.utils.bimapRx
import org.funktionale.either.Either
import org.funktionale.either.merge
import rx.Observable
import rx.schedulers.Schedulers

/**
 * Created by frenchdonuts on 5/12/2016.
 */

class RxActions(val rdb: RDB<AppState>, val todoAppDBRepo: TodoAppDBRepo) {

    fun GetTasks(origin: String): RxEffect {
        val TAG = "RxActions.GetTasks, called by: $origin"
        return RxEffect(
                todoAppDBRepo.getTasksFromDB()
                        .bimapRx(
                                {
                                    Log.d("GetTasks Error", "$it")
                                    AddTasksFromRepo_(immutableListOf(), TAG)
                                },
                                { AddTasksFromRepo_(it.toImmutableList(), TAG) }
                        )
                        .map { it.merge() }
        )
    }

    /**
     * It seems that for all Entities held in AppState, the id field doesn't seem to be too important. This is b/c
     *  I can create, read, update, and delete (in-memory aka in-UI) any Entity w/o needing their id if I just have
     *  a reference to it. I do have a reference to it b/c I can just query RDB for it.
     * But, what about Entities w/ relationships to one another?
     *  Say, Artist -> Song. This is a one-to-many relationship - an Artist can have many Songs, but a Song only has one Artist.
     *  Say I have a List<Artist> and List<Song> in AppState. They start off as sharing all data - there are no copies.
     *   A Song in an Artists List<Song> references an Object inside of the List<Song> in AppState.
     *   Likewise, an Artist in a Song references an Object inside of the List<Artist> in AppState.
     *      Artist: {...songs: List<Song>...}
     *      Song: {...artist: Artist...}
     *   Now, let's run through some CRUD scenarios for each Data Model.
     *   Create Artist:
     *      We add a new Artist Object to our List<Artist> in AppState. This doesn't change anything for any Song Objects
     *       since they can only have one Artist - the one they already have.
     *   Create Song:
     *      We add a new Song Object to our List<Artist> in AppState. Here, we have to be careful that our Reducers handle
     *       this properly: we need to add that SAME Song Object to the List<Song> of the appropriate Artist.
     *   Reading our Data Models:
     *      What are the Songs of artist1: Artist?
     *      Who is the Artist of song1: Song?
     *      These Queries seem to work out find.
     *   Update Artist:
     *      An Artist change his name. artistObject.name -> "meta_world_peace". If we are to maintain the invariant that all
     *       Models are SHARED, there is pretty much only one reasonable way to handle this:
     *        1) Dispatch UpdateArtist(artist0 = artistObject, artist1 = artistObject.copy(name = "meta_world_peace"))
     *        2) Update the List<Song> in AppState. appState.songs.map { if (it.artist === artist0) artist1 else it }
     *        3) Create a new Artist Object: val artist1' = artist1.copy(songs = appState.songs.filter {...})
     *        4) Update the List<Song> in AppState. appState.songs.map { if (it.artist === artist1) artist1' else it }
     *        5) Update the List<Artist> in AppState. appState.artists.map { if (it === artist1) artist1' else it }
     *      Okay...the fact that it took about 20 min. to figure out this sequence of updates STRONGLY HINTS that we should
     *      stick to relying on ids instead of references.
     *
     * So then, what is the solution? Well...this id dilemma is really only a problem for the Create part of CRUD.
     *  AppState doesn't need the id of the Data Model to Read. And to know which entity to Update or Delete, we must
     *  already have the id!
     *  So how do we take care of the Create part? Simple - we simply
     * Actually this^ is false as explained below. If we show the new created Model(optimistic update), but the underlying
     *  State Model hasn't received the id from DB/Remote, and the User swipes-to-delete. What do we do? What does the Action
     *  we dispatch look like?
     *  Will we really have to two sets of Actions for each Data Model? One that Modifies by reference and one by id?
     *  The problem - stated in more general terms - is: how do we handle modifications to the underlying data when all
     *   our data repositories have not been synchronized?
     */
    /**
     * Since this is asynchronous, it is possible that some Action that is dispatched may run a Reducer that needs the Task.id field before
     *   it is properly updated. How would I prevent that from happening?
     *   Well, this may just be a clue that we shouldn't even be doing the optimistic update?
     *   We could add a second identifier field. One used by Core and one used by DB
     *
     * Example scenario where this could happen:
     *  1) AddTaskObservable dispatched w/ task1. Optimistic UI updated occurs.
     *  2) User swipes before task1 receives its id from DB. RemoveTask w/ task1 is dispatched.
     *  3) B/c of how the Reducer is implemented, it looks for the id field to look for the Task to remove.
     *  Soln: Make the id field of Task String? instead of String. This way, the Reducer can know to use the id or reference.
     *  Soln: Make sure the Reducer uses reference as opposed to structural equality. But how would this be possible if
     *         more than 1 dev is working on a project?
     *  Soln: For any CRUD Actions on Entities, we have one where we pass in an Object and one where we pass in an id
     *  Soln: We pass around Entities and check for equality on them by reference only. This is not an issue since UI
     *         components ask for whole Tasks anyways, not just the id. I think this is the preferred approach especially
     *         in an OO-language.
     *
     *  Another scenario(never mind; still seems to work out):
     *   1) AddTaskObservable dispatched w/ task1. Optimistic UI updated occurs.
     *   2) User clicks on task1, dispatching a ShowTaskDetail.
     *   Hmmm...I guest this still works since we already have the Task in-memory...
     *
     * The flow for Entity update always seems to be this:
     *  in-memory -> db, remote
     * The only time in-memory needs to RECEIVE data from db/remote to do its job (UI stuff) is in the beginning! After
     *   that, the UI models are COMPLETELY self-sufficient! This is another clear example of needing to differentiate
     *   between UI-state-models and Domain models!
     *
     *   But, what about when we need to tell the server to modify something w/ a global(world wide web) id?
     *      In this, case the only way you would even KNOW what to modify is if you've ALREADY fetched it (from the server).
     *      So you would be good in this case as well.
     */
    fun AddTask(title: String, description: String, origin: String): RxEffect {
        //
        val TAG = "RxActions.AddTask, called by: $origin"
        val task = Task(title = title, description = description)

        val saveTaskToDB = Observable.just(AddTask_(task))
                // Optimistic UI update
                .doOnNext { rdb.dispatch(it) }
                .flatMap {
                    todoAppDBRepo
                            .saveTask(task)
                            .map {
                                when (it) {
                                    is Either.Left -> {
                                        Log.d("RxActions.AddTask", "Error: ${it.l}")
                                        // Undo optimistic update. Toast User as well?
                                        RemoveTask_(task, TAG)
                                    }
                                    is Either.Right ->
                                        // Update Task w/ new id
                                        UpdateTask_(task, it.r, TAG)
                                }
                            }
                }

        return RxEffect(saveTaskToDB, TAG)
    }


    fun MarkTaskActive(task: Task, origin: String) = UpdateTask(task, task.copy(isComplete = false), origin)
    fun MarkTaskComplete(task: Task, origin: String) = UpdateTask(task, task.copy(isComplete = true), origin)

    fun UpdateTask(task0: Task, task1: Task, origin: String): RxEffect {
        //
        val TAG = "RxActions.UpdateTask, called by: $origin"

        val updateDBWithTask = Observable.just(Unit)
                // Optimistic UI update
                .doOnNext { rdb.dispatch(UpdateTask_(task0, task1, TAG)) }
                .flatMap {
                    todoAppDBRepo.updateTask(task0.id, task1)
                            .map {
                                when (it) {
                                    is Either.Left -> {
                                        Log.d("RxActions.UpdateTask", "Error: ${it.l}")
                                        // Undo optimistic update
                                        UpdateTask_(task1, task0, TAG)
                                    }
                                    is Either.Right ->
                                        // UI already updated
                                        DoNothing_
                                }
                            }
                }

        return RxEffect(updateDBWithTask, TAG)
    }
}
