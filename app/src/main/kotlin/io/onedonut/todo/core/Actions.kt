package io.onedonut.todo.core

import io.onedonut.re_droid.Action
import io.onedonut.todo.core.models.Task

/**
 * Created by frenchdonuts on 4/29/16.
 *
 * The synchronous, in-memory updates
 */
object DoNothing_ : Action
data class RestoreState_(val state: AppState, val origin: String) : Action

// -------- Task CRUD --------
// Needed to initialize in-memory List<Task>
data class AddTasksFromRepo_(val tasks: List<Task>, val origin: String) : Action

// For optimistic UI updates
data class AddTask_(val task: Task) : Action
data class UpdateTask_(val task0: Task, val task1: Task, val origin: String) : Action
//fun MarkTaskActive_(task: Task, origin: String) = UpdateTask_(task, task.copy(isComplete = false), origin)
//fun MarkTaskCompleted_(task: Task, origin: String) = UpdateTask_(task, task.copy(isComplete = true), origin)
data class RemoveTask_(val task: Task, val origin: String) : Action
// ---------------------------


// Navigation
data class NavigateToTasksScreen_(val origin: String) : Action
data class NavigateToAddTaskScreen_(val origin: String) : Action
data class NavigateToTaskDetailScreen_(val task: Task, val origin: String) : Action


// Toasting
data class UserToasted_(val origin: String) : Action
