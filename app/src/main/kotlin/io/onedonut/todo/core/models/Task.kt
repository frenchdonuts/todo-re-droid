package io.onedonut.todo.core.models

import paperparcel.PaperParcel
import paperparcel.PaperParcelable
import java.util.*

/**
 * Created by frenchdonuts on 5/10/2016.
 */
@PaperParcel
data class Task(val globalId: String = "",
                val title: String = "",
                val description: String = "",
                val isComplete: Boolean = false) : PaperParcelable {

    companion object {
        @JvmField val CREATOR = PaperParcelTask.CREATOR
    }

    val id: String
        get() {
            if (globalId.equals(""))
                return tempId
            else
                return globalId
        }

    @delegate:Transient val tempId: String by lazy { UUID.randomUUID().toString() }
}
